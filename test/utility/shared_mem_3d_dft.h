//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef S3DFT_SHARED_MEM_3D_DFT_H
#define S3DFT_SHARED_MEM_3D_DFT_H

/// \file This file contains a function which performs the 3D-DFT of provided data using shared memory parallelization.

#include "matrix.h"
#include "tensor.h"


namespace s3dft
{
namespace test
{

template<class Treal_t>
matrix<Treal_t> create_coeff_matrix(const std::size_t& n);

template<class Treal_t>
struct scratch_cache
{
    tensor<Treal_t> tens0;
    tensor<Treal_t> tens1;
};

template<class Treal_t>
scratch_cache<Treal_t> create_scratch_cache(const std::size_t& n);

template<class Treal_t>
void execute(tensor<Treal_t>& data, const matrix<Treal_t>& coeff, scratch_cache<Treal_t>& scache);

} // namespace test
} // namespace s3dft

#endif
