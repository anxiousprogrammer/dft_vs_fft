//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation and explicit instantiations of 'read_tensor_block.h'.

#include "read_tensor_block.h"

#include <sstream> // std::stringstream
#include <fstream> // std::ifstream


namespace s3dft
{
namespace test
{

//+//////////////////////////////////////////////
// Implementation
//+//////////////////////////////////////////////

template<class Treal_t>
tensor<Treal_t> read_tensor_block(const std::string& file_name, const std::size_t& n)
{
    // open the file
    auto in_file = std::ifstream(file_name);
    if (!in_file.is_open())
    {
        throw std::runtime_error("read_data_pack_could_not_open_file_error");
    }
    
    // read the file line-by-line
    auto tens = tensor<Treal_t>(n);
    auto extraction_counter = std::size_t{};
    for (auto line_counter = std::size_t{}; in_file.good() && line_counter < n * n; ++line_counter)
    {
        // get the next line
        auto line = std::string{};
        std::getline(in_file, line);
        
        // get the pointer to the slice's data
        auto* tens_data = tens.get_slice_data(line_counter / n) + line_counter % n * n;
        
        // extract n complex values
        auto line_extractor = std::stringstream(line);
        for (auto value_counter = std::size_t{}; value_counter < n; ++value_counter)
        {
            // extract a paranthesis
            auto ch = char{};
            line_extractor >> ch;
            if (ch != '(')
            {
                throw std::runtime_error("read_data_pack_opening_paranthesis_missing_error");
            }
            
            // next, extract a value
            line_extractor >> tens_data->real;
            if (line_extractor.fail())
            {
                throw std::runtime_error("read_data_pack_could_not_extract_real_component_error");
            }
            
            // then, extract a comma
            line_extractor >> ch;
            if (ch != ',')
            {
                throw std::runtime_error("read_data_pack_comma_missing_error");
            }
            
            // next, extract another value
            line_extractor >> tens_data->imag;
            if (line_extractor.fail())
            {
                throw std::runtime_error("read_data_pack_could_not_extract_imag_component_error");
            }
            
            // finally, extract a paranthesis
            line_extractor >> ch;
            if (ch != ')')
            {
                throw std::runtime_error("read_data_pack_closing_paranthesis_missing_error");
            }
            
            // increment the data-point and the extraction counter
            ++tens_data;
            ++extraction_counter;
        }
    }
    
    // post-conditions
    if (extraction_counter != n * n * n)
    {
        throw std::runtime_error("read_data_pack_could_not_extract_all_datapoints_error");
    }
    return tens;
}


//+//////////////////////////////////////////////
// Explicit instantiation
//+//////////////////////////////////////////////

template tensor<double> read_tensor_block<double>(const std::string&, const std::size_t&);
template tensor<float> read_tensor_block<float>(const std::string&, const std::size_t&);

} // namespace test
} // namespace s3dft

// END OF IMPLEMENTATION
