//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the tests for the shared memory parallel 3D-DFT implementation, which is used to validate the
/// distributed memory algorithm.

#include "utility/shared_mem_3d_dft.h"
#include "utility/read_tensor_block.h"

#include "gtest/gtest.h"

#include <cmath> // std::cos, std::sin, std::atan
#include <filesystem> // std::filesystem::is_directory
#include <stdexcept> // std::runtime_error

#ifndef S3DFT_CURRENT_BINARY_DIR
#error "Current binary directory could not be determined!"
#endif


using namespace s3dft;

// TODO: test the functionality for sp
TEST(testsuite_serial, fftw_forward)
{
    //+/////////////////
    // Data retrieval
    //+/////////////////
    
    // check if path is OK
    const auto path_to_binary = std::string(S3DFT_CURRENT_BINARY_DIR);
    if (!std::filesystem::is_directory(path_to_binary))
    {
        throw std::runtime_error("test_could_not_find_current_binary_path_error");
    }
    
    // data decomposition
    const auto n = std::size_t{37};
    auto data = test::read_tensor_block<double>(path_to_binary + "/data/input_data.txt", n);
    
    //+/////////////////
    // Implementation
    //+/////////////////
    
    const auto coeff = test::create_coeff_matrix<double>(n);
    auto scache = test::create_scratch_cache<double>(n);
    test::execute(data, coeff, scache);
    const auto& result = data;
    
    //+/////////////////
    // Validation
    //+/////////////////
    
    // read the solution
    const auto solution = test::read_tensor_block<double>(path_to_binary + "/data/solution.txt", n);
    
    // compare the results
    ASSERT_EQ(solution.size(), result.size());
    const auto tolerance = 1E-11;
    for (auto d_index = std::size_t{}; d_index < solution.size(); ++d_index)
    {
        const auto& v1 = solution.data()[d_index];
        const auto& v2 = result.data()[d_index];
        ASSERT_NEAR(v1.real, v2.real, tolerance);
        ASSERT_NEAR(v1.imag, v2.imag, tolerance);
    }
}

// END OF TESTFILE
