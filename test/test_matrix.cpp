//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains test(s) for 'matrix'.

#include "matrix.h"

#include "utility/complex_op.h"

#include "gtest/gtest.h"


using namespace s3dft;
using namespace s3dft::test;

template<class Treal_t>
void test_lifecycle1()
{
    // test the constructibility
    auto test_matrix = matrix<Treal_t>(2);
    
    // fail cases
    #ifndef NDEBUG
    ASSERT_THROW(matrix<Treal_t>(0), std::invalid_argument);
    #endif
}

TEST(testsuite_matrix, lifecycle1_sp)
{
    test_lifecycle1<float>();
}

TEST(testsuite_matrix, lifecycle1_dp)
{
    test_lifecycle1<double>();
}


template<class Treal_t>
void test_access()
{
    // create a test instance
    auto test_matrix = matrix<Treal_t>(3);
    ASSERT_EQ(test_matrix.size(), 9);
    for (auto index = std::size_t{}; index < test_matrix.size(); ++index)
    {
        test_matrix.data()[index] = {static_cast<Treal_t>(index + 2), static_cast<Treal_t>(index + 2)};
    }
    
    // size
    ASSERT_EQ(test_matrix.get_n(), 3);
    
    // contents using pointer (const-qualified)
    ASSERT_NE(test_matrix.data(), nullptr);
    ASSERT_TRUE(equals(test_matrix.data()[0], {static_cast<Treal_t>(2.0), static_cast<Treal_t>(2.0)}));
    ASSERT_NE(test_matrix.data() + 1, nullptr);
    ASSERT_TRUE(equals(test_matrix.data()[1], {static_cast<Treal_t>(3.0), static_cast<Treal_t>(3.0)}));
    ASSERT_NE(test_matrix.data() + 2, nullptr);
    ASSERT_TRUE(equals(test_matrix.data()[2], {static_cast<Treal_t>(4.0), static_cast<Treal_t>(4.0)}));
    ASSERT_NE(test_matrix.data() + 3, nullptr);
    ASSERT_TRUE(equals(test_matrix.data()[3], {static_cast<Treal_t>(5.0), static_cast<Treal_t>(5.0)}));
    ASSERT_NE(test_matrix.data() + 4, nullptr);
    ASSERT_TRUE(equals(test_matrix.data()[4], {static_cast<Treal_t>(6.0), static_cast<Treal_t>(6.0)}));
    ASSERT_NE(test_matrix.data() + 5, nullptr);
    ASSERT_TRUE(equals(test_matrix.data()[5], {static_cast<Treal_t>(7.0), static_cast<Treal_t>(7.0)}));
    ASSERT_NE(test_matrix.data() + 6, nullptr);
    ASSERT_TRUE(equals(test_matrix.data()[6], {static_cast<Treal_t>(8.0), static_cast<Treal_t>(8.0)}));
    ASSERT_NE(test_matrix.data() + 7, nullptr);
    ASSERT_TRUE(equals(test_matrix.data()[7], {static_cast<Treal_t>(9.0), static_cast<Treal_t>(9.0)}));
    ASSERT_NE(test_matrix.data() + 8, nullptr);
    ASSERT_TRUE(equals(test_matrix.data()[8], {static_cast<Treal_t>(10.0), static_cast<Treal_t>(10.0)}));
    
    // contents using pointer (not const-qualified)
    for (auto index = std::size_t{}; index < test_matrix.size(); ++index)
    {
        test_matrix.data()[index] = static_cast<Treal_t>(2.0) * test_matrix.data()[index];
    }
    ASSERT_TRUE(equals(test_matrix.data()[0], {static_cast<Treal_t>(4.0), static_cast<Treal_t>(4.0)}));
    ASSERT_TRUE(equals(test_matrix.data()[1], {static_cast<Treal_t>(6.0), static_cast<Treal_t>(6.0)}));
    ASSERT_TRUE(equals(test_matrix.data()[2], {static_cast<Treal_t>(8.0), static_cast<Treal_t>(8.0)}));
    ASSERT_TRUE(equals(test_matrix.data()[3], {static_cast<Treal_t>(10.0), static_cast<Treal_t>(10.0)}));
    ASSERT_TRUE(equals(test_matrix.data()[4], {static_cast<Treal_t>(12.0), static_cast<Treal_t>(12.0)}));
    ASSERT_TRUE(equals(test_matrix.data()[5], {static_cast<Treal_t>(14.0), static_cast<Treal_t>(14.0)}));
    ASSERT_TRUE(equals(test_matrix.data()[6], {static_cast<Treal_t>(16.0), static_cast<Treal_t>(16.0)}));
    ASSERT_TRUE(equals(test_matrix.data()[7], {static_cast<Treal_t>(18.0), static_cast<Treal_t>(18.0)}));
    ASSERT_TRUE(equals(test_matrix.data()[8], {static_cast<Treal_t>(20.0), static_cast<Treal_t>(20.0)}));
    
    // contents using operator() (const-qualified)
    ASSERT_TRUE(equals(test_matrix(0, 0), {static_cast<Treal_t>(4.0), static_cast<Treal_t>(4.0)}));
    ASSERT_TRUE(equals(test_matrix(0, 1), {static_cast<Treal_t>(6.0), static_cast<Treal_t>(6.0)}));
    ASSERT_TRUE(equals(test_matrix(0, 2), {static_cast<Treal_t>(8.0), static_cast<Treal_t>(8.0)}));
    ASSERT_TRUE(equals(test_matrix(1, 0), {static_cast<Treal_t>(10.0), static_cast<Treal_t>(10.0)}));
    ASSERT_TRUE(equals(test_matrix(1, 1), {static_cast<Treal_t>(12.0), static_cast<Treal_t>(12.0)}));
    ASSERT_TRUE(equals(test_matrix(1, 2), {static_cast<Treal_t>(14.0), static_cast<Treal_t>(14.0)}));
    ASSERT_TRUE(equals(test_matrix(2, 0), {static_cast<Treal_t>(16.0), static_cast<Treal_t>(16.0)}));
    ASSERT_TRUE(equals(test_matrix(2, 1), {static_cast<Treal_t>(18.0), static_cast<Treal_t>(18.0)}));
    ASSERT_TRUE(equals(test_matrix(2, 2), {static_cast<Treal_t>(20.0), static_cast<Treal_t>(20.0)}));
    
    // contents using operator() (not const-qualified)
    for (auto i1 = std::size_t{}; i1 < test_matrix.get_n(); ++i1)
    {
        for (auto i2 = std::size_t{}; i2 < test_matrix.get_n(); ++i2)
        {
            test_matrix(i1, i2) = static_cast<Treal_t>(0.5) * test_matrix(i1, i2);
        }
    }
    ASSERT_TRUE(equals(test_matrix(0, 0), {static_cast<Treal_t>(2.0), static_cast<Treal_t>(2.0)}));
    ASSERT_TRUE(equals(test_matrix(0, 1), {static_cast<Treal_t>(3.0), static_cast<Treal_t>(3.0)}));
    ASSERT_TRUE(equals(test_matrix(0, 2), {static_cast<Treal_t>(4.0), static_cast<Treal_t>(4.0)}));
    ASSERT_TRUE(equals(test_matrix(1, 0), {static_cast<Treal_t>(5.0), static_cast<Treal_t>(5.0)}));
    ASSERT_TRUE(equals(test_matrix(1, 1), {static_cast<Treal_t>(6.0), static_cast<Treal_t>(6.0)}));
    ASSERT_TRUE(equals(test_matrix(1, 2), {static_cast<Treal_t>(7.0), static_cast<Treal_t>(7.0)}));
    ASSERT_TRUE(equals(test_matrix(2, 0), {static_cast<Treal_t>(8.0), static_cast<Treal_t>(8.0)}));
    ASSERT_TRUE(equals(test_matrix(2, 1), {static_cast<Treal_t>(9.0), static_cast<Treal_t>(9.0)}));
    ASSERT_TRUE(equals(test_matrix(2, 2), {static_cast<Treal_t>(10.0), static_cast<Treal_t>(10.0)}));
    
    // error cases
    #ifndef NDEBUG
    ASSERT_THROW(test_matrix(0, 3), std::invalid_argument);
    ASSERT_THROW(test_matrix(3, 0), std::invalid_argument);
    #endif
}

TEST(testsuite_matrix, access_sp)
{
    test_access<float>();
}

TEST(testsuite_matrix, access_dp)
{
    test_access<double>();
}


template<class Treal_t>
void test_lifecycle2()
{
    // create a test instance
    auto test_matrix = matrix<Treal_t>(2);
    ASSERT_EQ(test_matrix.size(), 4);
    for (auto index = std::size_t{}; index < test_matrix.size(); ++index)
    {
        test_matrix.data()[index] = {static_cast<Treal_t>(index + 1), static_cast<Treal_t>(index + 1)};
    }
    
    // test lambda
    const auto test_contents = [&test_matrix](const auto& matrix)
    {
        ASSERT_EQ(test_matrix.get_n(), 2);
        ASSERT_EQ(test_matrix.size(), 4);
        ASSERT_TRUE(equals(test_matrix(0, 0), {static_cast<Treal_t>(1.0), static_cast<Treal_t>(1.0)}));
        ASSERT_TRUE(equals(test_matrix(0, 1), {static_cast<Treal_t>(2.0), static_cast<Treal_t>(2.0)}));
        ASSERT_TRUE(equals(test_matrix(1, 0), {static_cast<Treal_t>(3.0), static_cast<Treal_t>(3.0)}));
        ASSERT_TRUE(equals(test_matrix(1, 1), {static_cast<Treal_t>(4.0), static_cast<Treal_t>(4.0)}));
    };
    
    // copy constructor
    auto test_matrix2 = test_matrix;
    test_contents(test_matrix2);
    
    // move constructor
    const auto test_matrix3 = matrix(std::move(test_matrix2)); // forced move constructor
    test_contents(test_matrix3);
    
    // copy operator
    test_matrix2 = test_matrix3; // forced copy operator
    test_contents(test_matrix2);
    test_contents(test_matrix3);
    
    // move operator
    const auto test_matrix4 = std::move(test_matrix2); // forced move operator
    test_contents(test_matrix2);
    ASSERT_EQ(test_matrix2.get_n(), 0);
    ASSERT_EQ(test_matrix2.size(), 0);
    #ifndef NDEBUG
    ASSERT_THROW(test_matrix2(0, 0), std::invalid_argument);
    #endif
}

TEST(testsuite_matrix, lifecycle2_sp)
{
    test_lifecycle2<float>();
}

TEST(testsuite_matrix, lifecycle2_dp)
{
    test_lifecycle2<double>();
}

// END OF TESTFILE
