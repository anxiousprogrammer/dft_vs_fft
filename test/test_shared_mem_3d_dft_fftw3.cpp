//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the tests for the shared memory parallel 3D-DFT implementation, which is used to validate the
/// distributed memory algorithm, using the FFTW3 as a reference.

#include "implementation/schedule_thread_work.h"

#include "utility/shared_mem_3d_dft.h"
#include "utility/core_count.h"

#include "fftw/fftw3.h"

#include "gtest/gtest.h"

#include <omp.h>

#include <cstdlib> // std::rand
#include <cmath> // std::sin


using namespace s3dft;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// helper functions
//+////////////////////////////////////////////////////////////////////////////////////////////////

tensor<double> get_random_data(const std::size_t& n)
{
    auto result = tensor<double>(n);
    for (auto index = std::size_t{}; index < result.size(); ++index)
    {
        const auto rand_value = []()
        {
            return (std::rand() % 2 == 0 ? 1.0 : -1.0) * std::sin(static_cast<double>(std::rand() % 10 + 1));
        };
        result.data()[index] = {rand_value(), rand_value()};
    }
    
    return result;
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// tester function
//+////////////////////////////////////////////////////////////////////////////////////////////////

void run_test(const std::size_t& n, const double& tolerance)
{
    //+/////////////////
    // get random data
    //+/////////////////
    
    const auto input_data = get_random_data(n);
    
    //+/////////////////
    // FFTW3
    //+/////////////////
    
    // allocate data
    auto* local_data = fftw_alloc_complex(input_data.size());
    if (local_data == nullptr)
    {
        std::cerr << "Fatal error: test data could not be allocated for!" << std::endl;
        throw std::runtime_error("run_test_fftw_alloc_complex_error");
    }
    
    // copy input data
    for (auto index = std::size_t{}; index < input_data.size(); ++index)
    {
        local_data[index][0] = input_data.data()[index].real;
        local_data[index][1] = input_data.data()[index].imag;
    }
    
    // create a plan
    auto* plan = fftw_plan_dft_3d(n, n, n, local_data, local_data, FFTW_FORWARD, FFTW_ESTIMATE);
    if (plan == NULL)
    {
        std::cerr << "Fatal error: fftw_plan could not be created!" << std::endl;
        throw std::runtime_error("run_test_fftw_plan_dft_3d_error");
    }
    
    // run FFTW3
    fftw_execute(plan);
    
    //+/////////////////
    // S3DFT
    //+/////////////////
    
    // setup
    const auto coeff = test::create_coeff_matrix<double>(n);
    auto scache = test::create_scratch_cache<double>(n);
    
    // copy input data
    auto s3dft_io = tensor<double>(n);
    #pragma omp parallel
    {
        const auto work_indices = schedule_thread_work(input_data.get_n(), 0);
        tensor_copy(input_data, s3dft_io, work_indices);
    }
    
    // run
    test::execute(s3dft_io, coeff, scache);
    
    //+/////////////////
    // validate
    //+/////////////////
    
    for (auto index = std::size_t{}; index < input_data.size(); ++index)
    {
        ASSERT_NEAR(local_data[index][0], s3dft_io.data()[index].real, tolerance);
        ASSERT_NEAR(local_data[index][1], s3dft_io.data()[index].imag, tolerance);
    }
    
    //+/////////////////
    // FFTW3 cleanup
    //+/////////////////
    
    fftw_destroy_plan(plan);
    fftw_free(local_data);
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// tests
//+////////////////////////////////////////////////////////////////////////////////////////////////

class shared_mem_3d_dft_fftw3 : public ::testing::Test
{
protected:

    //+//////////////////////////////////////////
    // lifecycle
    //+//////////////////////////////////////////
    
    shared_mem_3d_dft_fftw3()
    {
        // set the number of OpenMP threads
        omp_set_num_threads(test::g_core_count);
        
        // init threads for fftw
        if (fftw_init_threads() == 0)
        {
            std::cerr << "Error: FFTW could not initialize threads! The program will now exit." << std::endl;
            std::abort();
        }
        const auto thread_count = omp_get_max_threads();
        fftw_plan_with_nthreads(thread_count);
    }

    ~shared_mem_3d_dft_fftw3() override
    {
        fftw_cleanup_threads();
    }
};

TEST(testsuite_serial_fftw3, small_prime1)
{
    run_test(47, 1E-10);
}

TEST(testsuite_serial_fftw3, small_even2)
{
    run_test(90, 1E-9);
}

TEST(testsuite_serial_fftw3, large_prime3)
{
    run_test(191, 1E-8);
}

// END OF TESTFILE
