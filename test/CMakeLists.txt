# find gTest
if (BUILD_TESTS OR BUILD_TEST_AGAINST_FFTW)
    
    find_package(GTest REQUIRED)
    find_package(Threads)
endif()

# shared memory algorithm
add_library(shared_mem_3d_dft utility/shared_mem_3d_dft.cpp)
target_include_directories(shared_mem_3d_dft PRIVATE ${MKL_INCLUDE_DIR} ${PROJ_INCLUDE_DIR})
target_compile_options(shared_mem_3d_dft PRIVATE ${OPTIMIZATION_FLAGS} ${OpenMP_CXX_FLAGS})
target_link_directories(shared_mem_3d_dft PRIVATE ${MKL_LIBRARY_DIR})
target_link_libraries(shared_mem_3d_dft PRIVATE s3dft ${MKL_LIBRARIES} ${OpenMP_CXX_FLAGS})

# FFTW3 requirement
if (BUILD_TEST_AGAINST_FFTW OR BUILD_MPI_TESTS)

    if (NOT DEFINED ENV{FFTW3_ROOT})
        message(FATAL_ERROR "-- Please set the env. variable FFTW3_ROOT to point to the base directory of the FFTW3 library.")
    endif()
    set(FFTW3_INCLUDE_DIR $ENV{FFTW3_ROOT}/include)
    set(FFTW3_LIBRARY_DIR $ENV{FFTW3_ROOT}/lib)
    set(FFTW3_LIBRARIES libfftw3_omp.a libfftw3_threads.a libfftw3.a)
    set(FFTW3_MPI_LIBRARIES libfftw3_mpi.a libfftw3_omp.a libfftw3.a)
endif()

# gTest-based unit-tests
if (BUILD_TESTS)
    
    # test: proc_plan
    add_executable(test_proc_plan test_proc_plan.cpp)
    target_include_directories(test_proc_plan PRIVATE ${GTEST_INCLUDE_DIRS} ${PROJ_INCLUDE_DIR})
    target_compile_options(test_proc_plan PRIVATE ${OPTIMIZATION_FLAGS})
    target_link_libraries(test_proc_plan PRIVATE s3dft ${GTEST_BOTH_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})
    
    # test: test_schedule_thread_work
    add_executable(test_schedule_thread_work test_schedule_thread_work.cpp)
    target_include_directories(test_schedule_thread_work PRIVATE ${GTEST_INCLUDE_DIRS} ${PROJ_INCLUDE_DIR})
    target_compile_options(test_schedule_thread_work PRIVATE ${OPTIMIZATION_FLAGS} ${OpenMP_CXX_FLAGS})
    target_link_libraries(test_schedule_thread_work PRIVATE s3dft ${GTEST_BOTH_LIBRARIES} ${OpenMP_CXX_FLAGS} ${CMAKE_THREAD_LIBS_INIT})
    
    # test: matrix
    add_executable(test_matrix test_matrix.cpp)
    target_include_directories(test_matrix PRIVATE ${GTEST_INCLUDE_DIRS} ${PROJ_INCLUDE_DIR})
    target_compile_options(test_matrix PRIVATE ${OPTIMIZATION_FLAGS})
    target_link_libraries(test_matrix PRIVATE s3dft ${GTEST_BOTH_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})
    
    # test: tensor
    add_executable(test_tensor test_tensor.cpp)
    target_include_directories(test_tensor PRIVATE ${GTEST_INCLUDE_DIRS} ${PROJ_INCLUDE_DIR})
    target_compile_options(test_tensor PRIVATE ${OPTIMIZATION_FLAGS} ${OpenMP_CXX_FLAGS})
    target_link_libraries(test_tensor PRIVATE s3dft ${GTEST_BOTH_LIBRARIES} ${OpenMP_CXX_FLAGS} ${CMAKE_THREAD_LIBS_INIT})
    
    # test: shared_mem_tensor_matrix_math
    add_executable(test_shared_mem_tensor_matrix_math test_shared_mem_tensor_matrix_math.cpp)
    target_include_directories(test_shared_mem_tensor_matrix_math PRIVATE ${MKL_INCLUDE_DIR} ${GTEST_INCLUDE_DIRS} ${PROJ_INCLUDE_DIR})
    target_compile_options(test_shared_mem_tensor_matrix_math PRIVATE ${OPTIMIZATION_FLAGS} ${OpenMP_CXX_FLAGS})
    target_link_directories(test_shared_mem_tensor_matrix_math PRIVATE ${MKL_LIBRARY_DIR})
    target_link_libraries(test_shared_mem_tensor_matrix_math PRIVATE s3dft ${MKL_LIBRARIES} ${GTEST_BOTH_LIBRARIES} ${OpenMP_CXX_FLAGS} ${CMAKE_THREAD_LIBS_INIT})
    
    # test: transpose
    add_executable(test_transpose test_transpose.cpp)
    target_include_directories(test_transpose PRIVATE ${GTEST_INCLUDE_DIRS} ${PROJ_INCLUDE_DIR})
    target_compile_options(test_transpose PRIVATE ${OPTIMIZATION_FLAGS} ${OpenMP_CXX_FLAGS})
    target_link_libraries(test_transpose PRIVATE s3dft ${GTEST_BOTH_LIBRARIES} ${OpenMP_CXX_FLAGS} ${CMAKE_THREAD_LIBS_INIT})
    
    # test: shared memory parallel 3D-DFT
    add_executable(test_shared_mem_3d_dft test_shared_mem_3d_dft.cpp utility/read_tensor_block.cpp)
    target_include_directories(test_shared_mem_3d_dft PRIVATE ${MKL_INCLUDE_DIR} ${GTEST_INCLUDE_DIRS} ${PROJ_INCLUDE_DIR})
    target_compile_options(test_shared_mem_3d_dft PRIVATE ${OPTIMIZATION_FLAGS} ${OpenMP_CXX_FLAGS})
    target_link_directories(test_shared_mem_3d_dft PRIVATE ${MKL_LIBRARY_DIR})
    target_link_libraries(test_shared_mem_3d_dft PRIVATE s3dft shared_mem_3d_dft ${MKL_LIBRARIES} ${GTEST_BOTH_LIBRARIES} ${OpenMP_CXX_FLAGS} ${CMAKE_THREAD_LIBS_INIT})
    target_compile_definitions(test_shared_mem_3d_dft PRIVATE "S3DFT_CURRENT_BINARY_DIR=\"${CMAKE_CURRENT_BINARY_DIR}\"")
    add_custom_command(TARGET test_shared_mem_3d_dft POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/data ${CMAKE_CURRENT_BINARY_DIR}/data)
endif()

# test: shared memory parallel 3D-DFT (against FFTW3)
if (BUILD_TEST_AGAINST_FFTW)
    
    # build recipe
    add_executable(test_shared_mem_3d_dft_fftw3 test_shared_mem_3d_dft_fftw3.cpp)
    target_include_directories(test_shared_mem_3d_dft_fftw3 PRIVATE ${FFTW3_INCLUDE_DIR} ${MKL_INCLUDE_DIR} ${GTEST_INCLUDE_DIRS} ${PROJ_INCLUDE_DIR})
    target_compile_options(test_shared_mem_3d_dft_fftw3 PRIVATE ${OPTIMIZATION_FLAGS} ${OpenMP_CXX_FLAGS})
    target_link_directories(test_shared_mem_3d_dft_fftw3 PRIVATE ${MKL_LIBRARY_DIR} ${FFTW3_LIBRARY_DIR})
    target_link_libraries(test_shared_mem_3d_dft_fftw3 PRIVATE s3dft shared_mem_3d_dft ${FFTW3_LIBRARIES} ${MKL_LIBRARIES} ${GTEST_BOTH_LIBRARIES} ${OpenMP_CXX_FLAGS})
endif()

# MPI-based tests
if (BUILD_MPI_TESTS)
    
    # test: distr_mem_tensor_matrix_math
    add_executable(test_distr_mem_tensor_matrix_math test_distr_mem_tensor_matrix_math.cpp)
    target_include_directories(test_distr_mem_tensor_matrix_math PRIVATE ${MKL_INCLUDE_DIR} ${MPI_INCLUDE_PATH} ${PROJ_INCLUDE_DIR})
    target_compile_options(test_distr_mem_tensor_matrix_math PRIVATE ${OPTIMIZATION_FLAGS} ${OpenMP_CXX_FLAGS})
    target_link_directories(test_distr_mem_tensor_matrix_math PRIVATE ${MKL_LIBRARY_DIR})
    target_link_libraries(test_distr_mem_tensor_matrix_math PRIVATE s3dft ${MKL_LIBRARIES} ${MPI_CXX_LIBRARIES} ${OpenMP_CXX_FLAGS})
    
    # test: API function
    add_executable(test_api test_api.cpp)
    target_include_directories(test_api PRIVATE ${MKL_INCLUDE_DIR} ${MPI_INCLUDE_PATH} ${PROJ_INCLUDE_DIR})
    target_compile_options(test_api PRIVATE ${OPTIMIZATION_FLAGS} ${OpenMP_CXX_FLAGS})
    target_link_directories(test_api PRIVATE ${MKL_LIBRARY_DIR})
    target_link_libraries(test_api PRIVATE s3dft shared_mem_3d_dft ${MKL_LIBRARIES} ${MPI_CXX_LIBRARIES} ${OpenMP_CXX_FLAGS})
    
    # test: compare APIs
    add_executable(test_api_compare test_api_compare.cpp)
    target_include_directories(test_api_compare PRIVATE ${FFTW3_INCLUDE_DIR} ${MKL_INCLUDE_DIR} ${MPI_INCLUDE_PATH} ${PROJ_INCLUDE_DIR})
    target_compile_options(test_api_compare PRIVATE ${OPTIMIZATION_FLAGS} ${OpenMP_CXX_FLAGS})
    target_link_directories(test_api_compare PRIVATE ${FFTW3_LIBRARY_DIR} ${MKL_LIBRARY_DIR})
    target_link_libraries(test_api_compare PRIVATE s3dft ${FFTW3_MPI_LIBRARIES} ${MKL_LIBRARIES} ${MPI_CXX_LIBRARIES} ${OpenMP_CXX_FLAGS})
endif()

# if applicable, copy the run script
if (BUILD_TESTS AND BUILD_TEST_AGAINST_FFTW AND BUILD_MPI_TESTS)
    file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/utility/run_tests.sh DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/)
endif()
