//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation and explicit instantiations of 'transpose.h'.

#include "implementation/transpose.h"

#include <omp.h>

#include <utility> // std::size_t
#include <stdexcept> // std::invalid_argument


namespace s3dft
{

//+//////////////////////////////////////////////
// Implementation
//+//////////////////////////////////////////////

template<class Treal_t>
void transpose13(const tensor<Treal_t>& tens, tensor<Treal_t>& tens_tr)
{
    // preconditions
    #ifndef NDEBUG
    #pragma omp master
    {
        if (tens.get_n() == 0)
        {
            throw std::invalid_argument("transpose13_tensor_size_error");
        }
        if (tens_tr.get_n() != tens.get_n())
        {
            throw std::invalid_argument("transpose13_tensor_size_mismatch_error");
        }
    }
    #endif
    
    // renaming for convenience
    const auto& n = tens.get_n();
    
    // blocking constants
    constexpr auto block_size = 32; // TODO: automatization required here
    auto cache_buffer = std::array<complex<Treal_t>, block_size * block_size + 3>{};
    
    // loop
    const auto n_div = n / block_size * block_size;
    #pragma omp for schedule(static)
    for (auto i2 = std::size_t(0); i2 < n; ++i2)
    {        
        // blocked loop
        for (auto bi1 = std::size_t{}; bi1 < n_div; bi1 += block_size)
        {
            for (auto bi3 = std::size_t{}; bi3 < n_div; bi3 += block_size)
            {
                for (auto i1 = std::size_t{}; i1 < block_size; ++i1)
                {
                    for (auto i3 = std::size_t{}; i3 < block_size; ++i3)
                    {
                        cache_buffer[i3 * block_size + i1] = tens(bi1 + i1, i2, bi3 + i3);
                    }
                }
                
                for (auto i1 = std::size_t{}; i1 < block_size; ++i1)
                {
                    for (auto i3 = std::size_t{}; i3 < block_size; ++i3)
                    {
                        tens_tr(bi3 + i1, i2, bi1 + i3) = cache_buffer[i1 * block_size + i3];
                    }
                }
            }
        }
        
        // remainder block 1
        for (auto i1 = std::size_t{}; i1 < n_div; ++i1)
        {
            for (auto i3 = n_div; i3 < n; ++i3)
            {
                tens_tr(i3, i2, i1) = tens(i1, i2, i3);
            }
        }
        
        // remainder block 2
        for (auto i1 = n_div; i1 < n; ++i1)
        {
            for (auto i3 = std::size_t{}; i3 < n; ++i3)
            {
                tens_tr(i3, i2, i1) = tens(i1, i2, i3);
            }
        }
    }
}


template<class Treal_t>
void transpose23(const tensor<Treal_t>& tens, tensor<Treal_t>& tens_tr, const std::vector<std::size_t>& work_indices)
{
    // preconditions
    #ifndef NDEBUG
    #pragma omp master
    {
        if (tens.get_n() == 0)
        {
            throw std::invalid_argument("transpose23_tensor_size_error");
        }
        if (tens_tr.get_n() != tens.get_n())
        {
            throw std::invalid_argument("transpose23_tensor_size_mismatch_error");
        }
    }
    #endif
    
    // renaming for convenience
    const auto& n = tens.get_n();
    
    // cache-buffer
    constexpr auto block_size = 32; // TODO: automatization required here
    constexpr auto block_size_sq = block_size * block_size;
    auto cb = std::array<complex<Treal_t>, block_size_sq>{};
    
    // standard case: large matrix
    const auto n_div = n / block_size * block_size;
    for (const auto& i1 : work_indices)
    {
        // blocked loop
        for (auto bi2 = std::size_t{}; bi2 < n_div; bi2 += block_size)
        {
            for (auto bi3 = std::size_t{}; bi3 < n_div; bi3 += block_size)
            {
                for (auto i2 = std::size_t{}; i2 < block_size; ++i2)
                {
                    for (auto i3 = std::size_t{}; i3 < block_size; ++i3)
                    {
                        cb[i3 * block_size + i2] = tens(i1, bi2 + i2, bi3 + i3);
                    }
                }
                for (auto i2 = std::size_t{}; i2 < block_size; ++i2)
                {
                    for (auto i3 = std::size_t{}; i3 < block_size; ++i3)
                    {
                        tens_tr(i1, bi3 + i2, bi2 + i3) = cb[i2 * block_size + i3];
                    }
                }
            }
        }
        
        // remainder block 1
        for (auto i2 = std::size_t{}; i2 < n_div; ++i2)
        {
            for (auto i3 = n_div; i3 < n; ++i3)
            {
                tens_tr(i1, i3, i2) = tens(i1, i2, i3);
            }
        }
        
        // remainder block 2
        for (auto i2 = n_div; i2 < n; ++i2)
        {
            for (auto i3 = std::size_t{}; i3 < n; ++i3)
            {
                tens_tr(i1, i3, i2) = tens(i1, i2, i3);
            }
        }
    }
}


//+//////////////////////////////////////////////
// Explicit instantiation
//+//////////////////////////////////////////////

template void transpose13<float>(const tensor<float>&, tensor<float>&);
template void transpose13<double>(const tensor<double>&, tensor<double>&);

template void transpose23<float>(const tensor<float>&, tensor<float>&, const std::vector<std::size_t>&);
template void transpose23<double>(const tensor<double>&, tensor<double>&, const std::vector<std::size_t>&);

} // namespace s3dft

// END OF IMPLEMENTATION
