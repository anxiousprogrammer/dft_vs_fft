//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation and explicit instantiations of 'data_pack'.

#include "implementation/data_pack.h"

#include <algorithm> // std::fill, std::copy
#include <stdexcept> // std::runtime_error

#include <unistd.h> // sysconf
#include <stdlib.h> // posix_memalign


namespace s3dft
{

//+//////////////////////////////////////////////
// Implementation
//+//////////////////////////////////////////////

template<class Treal_t>
void data_pack<Treal_t>::allocate(const std::size_t& size)
{
    posix_memalign(reinterpret_cast<void**>(&data_), sysconf(_SC_PAGESIZE), size * sizeof(complex<Treal_t>));
    if (data_ == nullptr)
    {
        throw std::runtime_error("data_pack_allocate_could_not_allocate_memory_error");
    }
}


//+//////////////////////////////////////////////
// Lifecycle
//+//////////////////////////////////////////////

template<class Treal_t>
data_pack<Treal_t>::data_pack(const std::size_t& size)
{
    allocate(size);
    size_ = size;
}

template<class Treal_t>
data_pack<Treal_t>::data_pack(const std::size_t& size, const complex<Treal_t>& init_val)
{
    allocate(size);
    std::fill(data_, data_ + size, init_val);
    size_ = size;
}

template<class Treal_t>
data_pack<Treal_t>::data_pack(const data_pack& rhs)
{
    allocate(rhs.size_);
    std::copy(rhs.data_, rhs.data_ + rhs.size_, data_);
    size_ = rhs.size_;
}

template<class Treal_t>
data_pack<Treal_t>::data_pack(data_pack&& rhs)
{
    data_ = rhs.data_;
    size_ = rhs.size_;
    rhs.data_ = nullptr;
    rhs.size_ = {};
}

template<class Treal_t>
data_pack<Treal_t>& data_pack<Treal_t>::operator=(const data_pack& rhs)
{
    // self-assignment is a non-action
    if (&rhs == this)
    {
        return *this;
    }
    
    #ifndef NDEBUG
    // preconditions
    if (rhs.data_ == nullptr)
    {
        throw std::runtime_error("data_pack_operator=copy_nullptr_error");
    }
    #endif
    
    // free existing memory and reallocate if the size isn't the same
    if (size_ != rhs.size_)
    {
        if (data_ != nullptr)
        {
            std::free(data_);
        }
        allocate(rhs.size_);
    }
    
    // copy data
    std::copy(rhs.data_, rhs.data_ + rhs.size_, data_);
    size_ = rhs.size_;
    
    return *this;
}

template<class Treal_t>
data_pack<Treal_t>& data_pack<Treal_t>::operator=(data_pack&& rhs)
{
    // self-assignment is a non-action
    if (&rhs == this)
    {
        return *this;
    }
    
    #ifndef NDEBUG
    // preconditions
    if (rhs.data_ == nullptr)
    {
        throw std::runtime_error("data_pack_operator=move_nullptr_error");
    }
    #endif
    
    // free existing memory
    if (data_ != nullptr)
    {
        std::free(data_);
    }
    
    // copy data
    data_ = rhs.data_;
    size_ = rhs.size_;
    
    // invalidate rhs instance
    rhs.data_ = nullptr;
    rhs.size_ = {};
    
    return *this;
}

template<class Treal_t>
data_pack<Treal_t>::~data_pack()
{
    if (data_ != nullptr)
    {
        std::free(data_);
    }
    size_ = {};
}


//+//////////////////////////////////////////////
// Explicit instantiation
//+//////////////////////////////////////////////

template class data_pack<float>;
template class data_pack<double>;

} // namespace s3dft

// END OF IMPLEMENTATION
