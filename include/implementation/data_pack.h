//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef S3DFT_DATA_PACK_H
#define S3DFT_DATA_PACK_H

/// \file This file contains a construct which allocates and deallocates arrays for (DP/SP) complex numbers.

#include "implementation/complex.h"

#include <utility> // std::size_t


namespace s3dft
{

// forward declaration of template friends
template<class Treal_t> class matrix;
template<class Treal_t> class tensor;

/// class template for an array of dcomplex/scomplex
template<class Treal_t>
class data_pack
{
    // friends
    friend class matrix<Treal_t>;
    friend class tensor<Treal_t>;
    
    //+//////////////////////////////////////////
    // Member
    //+//////////////////////////////////////////
    
    complex<Treal_t>* data_ = nullptr;
    std::size_t size_ = {};
    
    
    //+//////////////////////////////////////////
    // Implementation
    //+//////////////////////////////////////////
    
    void allocate(const std::size_t& size);
    
    
public:

    //+//////////////////////////////////////////
    // Lifecycle
    //+//////////////////////////////////////////
    
    data_pack() = default;
    
    data_pack(const std::size_t& size);
    
    data_pack(const std::size_t& size, const complex<Treal_t>& init_val);
    
    data_pack(const data_pack& rhs);
    
    data_pack(data_pack&& rhs);
    
    data_pack& operator=(const data_pack& rhs);
    
    data_pack& operator=(data_pack&& rhs);
    
    ~data_pack();
};

} // namespace s3dft
#endif
