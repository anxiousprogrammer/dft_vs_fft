//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef S3DFT_TENSOR_MATRIX_MATH_H
#define S3DFT_TENSOR_MATRIX_MATH_H

/// \file This file contains function(s) which perform(s) matrix/tensor operations.

#include "matrix.h"
#include "tensor.h"

#include <mpi.h>

#include <vector> // std::vector


namespace s3dft
{
namespace shared_mem
{

// tensor-matrix multiplication
template<class Treal_t>
void tensor_matrix_mult(const tensor<Treal_t>& tens, const matrix<Treal_t>& mat, tensor<Treal_t>& result,
    const std::vector<std::size_t>& work_indices);

} // namespace shared_mem

namespace distr_mem
{

#define COMM_THREAD 0

// tensor-matrix multiplication
template<class Treal_t>
void tensor_matrix_mult(const std::size_t& p, MPI_Comm comm, const int& proc_rank, tensor<Treal_t>& tens_block,
    matrix<Treal_t>& mat_block, tensor<Treal_t>& result_block, tensor<Treal_t>& scratch_tens, matrix<Treal_t>& scratch_mat,
    const std::vector<std::size_t>& work_indices);

// transp. matrix-tensor multiplication
template<class Treal_t>
void transp_matrix_tensor_mult(const std::size_t& p, MPI_Comm comm, const int& proc_rank, matrix<Treal_t>& mat_block,
    tensor<Treal_t>& tens_block, tensor<Treal_t>& result_block, tensor<Treal_t>& scratch_tens, matrix<Treal_t>& scratch_mat,
    const std::vector<std::size_t>& work_indices);

// tansposed tensor (along direction 2)-matrix multiplication
template<class Treal_t>
void transp_tensor_matrix_mult(const std::size_t& p, MPI_Comm comm, const int& proc_rank, tensor<Treal_t>& tens_block,
    matrix<Treal_t>& mat_block, tensor<Treal_t>& result_block, tensor<Treal_t>& scratch_tens, matrix<Treal_t>& scratch_mat,
    const std::vector<std::size_t>& work_indices);

} // namespace distr_mem
} // namespace s3dft
#endif
