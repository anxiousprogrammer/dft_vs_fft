cmake_minimum_required(VERSION 3.16)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# options
option(BUILD_DEBUG_SECTIONS "Builds parts of the code which can be only turned on for debugging purposes" OFF)
option(BUILD_TESTS "The unit-tests of the project will be built (includes dependency on googletest)" ON)
option(BUILD_MPI_TESTS "The MPI-based unit-tests of the project will be built" ON)
option(BUILD_TEST_AGAINST_FFTW "A unit-test which compares the outputs of S3DFT and FFTW3 will be built (includes dependency on FFTW3)" ON)
option(USE_LIKWID_MARKER_API "The LIKWID Marker API will be used for performance counting" OFF)
option(BUILD_MKL_BENCHMARK "Builds the MKL distributed memory benchmark" ON)
option(BUILD_FFTW3_BENCHMARK "Builds the FFTW3 distributed memory benchmark" ON)
option(BUILD_FFTK_BENCHMARK "Builds the FFTK distributed memory benchmark" ON)
option(MBENCH_EVICT_CACHE_LINES "Evicts cache lines after data initialization in microbenchmarking programs" ON)
set(CACHE_LINE_SIZE "64" CACHE STRING "Uses the system cache line size in microbenchmarking programs for evicting cache lines")
set(MESSAGE_PACKET_SIZE "500" CACHE STRING "Decides the size of individual MPI messages in MiB")

# propagate the options
if (NOT BUILD_DEBUG_SECTIONS)
    add_compile_definitions("NDEBUG")
endif ()
if (NOT MESSAGE_PACKET_SIZE MATCHES "^[0-9]+$" OR ${MESSAGE_PACKET_SIZE} LESS 1 OR ${MESSAGE_PACKET_SIZE} GREATER 5000)
    message(FATAL_ERROR "An invalid MPI message packet size has been provided!")
endif()

# set the project name
project(s3dft VERSION 0.1.0)

# obtain information about the compiler
if (CMAKE_CXX_COMPILER_ID MATCHES "^GNU")
    set(GNU_COMPILER ON)
    set(INTEL_COMPILER OFF)
elseif (CMAKE_CXX_COMPILER_ID MATCHES "^Intel")
    set(GNU_COMPILER OFF)
    set(INTEL_COMPILER ON)
else()
    message(WARNING "Only Intel and GCC compiler are currently configured; compilation may not succeed!")
endif()

# at the moment, since libfftw3x_cdft_lp64 requires a fully Intel environment, we're going to stick to Intel
if (GNU_COMPILER)
    message(FATAL_ERROR "-- Only Intel compiler is supported until benchmarking of the distr. algo is done! Please set path to icpc.")
endif()

# find required OpenMP lib
find_package(OpenMP REQUIRED)
if (GNU_COMPILER)
    message("-- OpenMP Library located at: " ${OpenMP_gomp_LIBRARY})
elseif (INTEL_COMPILER)
    message("-- OpenMP Library located at: " ${OpenMP_iomp5_LIBRARY})
endif()

# find MPI
if (DEFINED ENV{EBROOTPSMPI})
    set(MPI_INCLUDE_DIR $ENV{EBROOTPSMPI}/include)
    set(MPI_CXX_LIBRARIES $ENV{EBROOTPSMPI}/lib/libmpi.so)
    message("-- ParaStationMPI Library located at: " ${MPI_CXX_LIBRARIES})
elseif(DEFINED ENV{I_MPI_ROOT})
    set(MPI_INCLUDE_DIR $ENV{I_MPI_ROOT}/include)
    set(MPI_CXX_LIBRARIES $ENV{I_MPI_ROOT}/lib/release/libmpi.so)
    message("-- IntelMPI Library located at: " ${MPI_CXX_LIBRARIES})
else()
    message(FATAL_ERROR "-- Only Intel MPI/ParaStationMPI are supported at this point of time.")
endif()

# find MKL
if (NOT DEFINED ENV{MKLROOT})
    message(FATAL_ERROR "-- Please run MKL's set-env-vars script before attempting to configure. Alternatively set env. variable MKLROOT to point to the base directory.")
endif()
set(MKL_INCLUDE_DIR $ENV{MKLROOT}/include)
set(MKL_LIBRARY_DIR $ENV{MKLROOT}/lib/intel64)
set(MKL_LIBRARIES libmkl_intel_lp64.so libmkl_sequential.so libmkl_core.so)

# common variables (top level)
set(PROJ_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/include)
set(OPTIMIZATION_FLAGS "-O3;-ffast-math;-march=native;-qopt-zmm-usage=high")
if (INTEL_COMPILER)
    set(WARNING_FLAGS "-Wall;-Wextra;-Werror")
elseif (GNU_COMPILER)
    set(WARNING_FLAGS "-fstack-protector-all;-O3;-Wall;-Wpedantic;\
-Wclobbered;-Wdeprecated-copy;-Wenum-conversion;-Wignored-qualifiers;-Wmissing-field-initializers;\
-Wsign-compare;-Wstring-compare;-Wredundant-move;-Wtype-limits;-Wuninitialized;-Wshift-negative-value;-Wunused-parameter;\
-Werror")
endif()
set(COMMON_COMPILE_FLAGS ${OPTIMIZATION_FLAGS} ${WARNING_FLAGS})

# Subprojects
add_subdirectory(src)
add_subdirectory(test)
add_subdirectory(benchmark)
