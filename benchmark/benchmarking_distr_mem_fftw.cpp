//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the benchmark program for the FFTW3 API function (maybe Intel or FFTW3 implementation).

#include "write_measurements.h"

#include "tixl_w_mpi.h"

#include "fftw/fftw3.h"
#include "fftw/fftw3-mpi.h"

#include <omp.h>
#include <mpi.h>

#include <fstream> // std::ofstream
#include <iostream> // std::cout
#include <cmath> // std::cbrt
#include <algorithm> // std::minmax
#include <cstring> // std::strlen
#include <cctype> // std::isdigit


using namespace s3dft;

namespace impl
{

class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t n_;
    const std::size_t local_size_;
    fftw_complex* local_data_ = nullptr;
    fftw_plan plan_ = {};
    
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& n, const std::size_t& local_size)
        : n_(n), local_size_(local_size) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocate data
        local_data_ = fftw_alloc_complex(local_size_);
        if (local_data_ == nullptr)
        {
            std::cerr << "Fatal error: test data could not be allocated for!" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
        }
        
        // create a plan
        plan_ = fftw_mpi_plan_dft_3d(n_, n_, n_, local_data_, local_data_, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE);
        if (plan_ == NULL)
        {
            std::cerr << "Fatal error: fftw_plan could not be created!" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
        }
    }
    
    void perform_experiment() final
    {
        fftw_execute(plan_);
    }
    
    void finish() final
    {
        fftw_destroy_plan(plan_);
        fftw_free(local_data_);
    }
};

} // namespace impl

int main(int argc, char** argv)
{
    //+/////////////////
    // MPI init
    //+/////////////////
    
    auto provided = int{};
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
    if (provided < MPI_THREAD_FUNNELED)
    {
        std::cerr << "Fatal error: Multi-threading support not available with the current MPI implementation." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // rank
    auto world_rank = int{};
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    
    // size
    auto world_size = int{};
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    
    //+/////////////////
    // extract the problem size
    // and filename of the measurements
    // log
    //+/////////////////
    
    if (argc < 2)
    {
        std::cerr << "Please provide the problem size as command line argument."
            << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    const auto n = static_cast<std::size_t>(std::stoul(argv[1]));
    
    //+/////////////////
    // extract 'p'
    //+/////////////////
    
    auto p = std::size_t{};
    if (argc < 3)
    {
        std::cerr << "Please provide the value of p as command line argument." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    else
    {
        auto is_number = true;
        for (auto index = std::size_t{}; index < std::strlen(argv[2]); ++index)
        {
            if (!std::isdigit(argv[2][index]))
            {
                is_number = false;
                break;
            }
        }
        if (!is_number)
        {
            std::cerr << "Fatal error: value of p must be a number." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
        else
        {
            p = std::stoul(argv[2]);
            if (p == 0 || p > world_size)
            {
                std::cerr << "Fatal error: invalid value of p provided." << std::endl;
                MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
            }
        }
    }
    
    //+/////////////////
    // FFTW3
    //+/////////////////
    
    // init threads for fftw
    if (fftw_init_threads() == 0)
    {
        std::cerr << "Fatal error: FFTW could not initialize threads! The program will now exit." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    const auto thread_count = omp_get_max_threads();
    fftw_plan_with_nthreads(thread_count);
    
    // init MPI
    fftw_mpi_init();
    
    // size of decomposition
    auto local_n0 = ptrdiff_t{};
    auto local_0_start = ptrdiff_t{};
    const auto local_size = fftw_mpi_local_size_3d(n, n, n, MPI_COMM_WORLD, &local_n0, &local_0_start);
    if (local_size == 0)
    {
        std::cerr << "Fatal error: FFTW/iMKL calculated a local size of 0 (domain decomposition failure)." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // report
    if (world_rank == 0)
    {
        #ifdef IS_MKL_BENCHMARK
        std::cout << "Starting runs for MKL with N=" << n << ", p=" << p << " (N_task=" << world_size << ").\n";
        #else
        std::cout << "Starting runs for FFTW3 with N=" << n << ", p=" << p << " (N_task=" << world_size << ").\n";
        #endif
        std::cout << "Thread count: " << omp_get_max_threads() << ".\n";
    }
    
    // perform the experiments
    const auto experiment_count = std::size_t{100}; // REMARK: used to be 250 but was cut down due to computational costs on cluster!
    auto ef = impl::exp_fn(n, local_size);
    const auto measurements = tixl::mpi_perform_experiments(MPI_COMM_WORLD, ef, experiment_count);
    const auto result = tixl::mpi_compute_statistics(MPI_COMM_WORLD, measurements);
    if (world_rank == 0)
    {
        #ifdef IS_MKL_BENCHMARK
        tixl::output_results("Results of the runs with MKL", result, 3 * 8 * n * n * n * n);
        benchmarking::write_measurements(measurements, std::string("mkl_measurements_p") + std::to_string(p)
            + std::string(".txt"));
        #else
        tixl::output_results("Results of the runs with FFTW3", result, 3 * 8 * n * n * n * n);
        benchmarking::write_measurements(measurements, std::string("fftw3_measurements_p") + std::to_string(p)
            + std::string(".txt"));
        #endif
    }
    
    // clean up
    fftw_cleanup_threads();
    
    // finalize MPI
    MPI_Finalize();
    
    return 0;
}
