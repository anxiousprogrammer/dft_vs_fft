//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the benchmark program of the communication in 'distr_mem::tensor_matrix_math'.

#include "implementation/schedule_thread_work.h"
#include "implementation/proc_plan.h"
#include "implementation/tensor_matrix_math.h"
#include "matrix.h"
#include "tensor.h"

#include "../set_first_touch.h"
#include "../write_measurements.h"
#include "message_packet_details.h"

#include "tixl_w_mpi.h"

#include <omp.h>
#include <mpi.h>

#include <iostream> // std::cout
#include <iomanip> // std::put_time
#include <stdexcept> // std::invalid_argument
#include <cmath> // std::cbrt
#include <string> // std::stoul
#include <vector> // std::vector
#include <chrono> // std::chrono::system_clock::to_time_t


using namespace s3dft;

//+//////////////////////////////////////////////
// specially adapted functionality from TiXL
//+//////////////////////////////////////////////

#define MAKE_PROGRESS_REPORTS

std::vector<std::vector<double>> mpi_perform_interleaved_experiments(MPI_Comm comm, tixl::experiment_functor& exp_fn,
    const std::size_t& exp_count, const unsigned int& interleave_count)
{
    // pre-conditions
    if (exp_count < 1)
    {
        throw std::runtime_error("mpi_perform_interleaved_experiments_exp_count_error");
    }
    auto is_init = int{};
    MPI_Initialized(&is_init);
    if (is_init != 1)
    {
        throw std::logic_error("mpi_perform_interleaved_experiments_mpi_not_initialized_error");
    }
    auto proc_rank = -1;
    if (MPI_Comm_rank(comm, &proc_rank) != MPI_SUCCESS)
    {
        throw std::runtime_error("mpi_perform_interleaved_experiments_mpi_comm_rank_error");
    }
    if (exp_count < 1)
    {
        throw std::runtime_error("mpi_perform_interleaved_experiments_exp_count_zero_error");
    }
    if (interleave_count < 1)
    {
        throw std::runtime_error("mpi_perform_interleaved_experiments_interleave_count_zero_error");
    }
    
    // perform warm-up runs
    const auto interleaved_exp_count = exp_count * interleave_count;
    const auto warm_up_count = interleaved_exp_count / 5;
    for (auto iter_counter = std::size_t{}; iter_counter < warm_up_count; ++iter_counter)
    {
        exp_fn.init();
        exp_fn.perform_experiment();
        exp_fn.finish();
    }
    
    // progress 1
    #ifdef MAKE_PROGRESS_REPORTS
    if (proc_rank == 0)
    {
        const auto time_stamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        std::cout << std::put_time(std::localtime(&time_stamp), "[%H:%M:%S]") << " warm-up completed." << "\n";
    }
    auto phase = 0;
    #endif
    
    // perform the timed experiments
    auto measurements = std::vector(interleave_count, std::vector<double>{});
    auto start_time_us = std::size_t{};
    for (auto exp_counter = std::size_t{}; exp_counter < interleaved_exp_count; ++exp_counter)
    {
        // init the data
        exp_fn.init();
        
        // perform a timed run
        MPI_Barrier(comm);
        if (proc_rank == 0)
        {
            start_time_us = omp_get_wtime() * 1E+6;
        }
        MPI_Barrier(comm);
        exp_fn.perform_experiment();
        MPI_Barrier(comm);
        if (proc_rank == 0)
        {
            const auto run_duration = omp_get_wtime() * 1E+6 - start_time_us;
            measurements[exp_counter % interleave_count].push_back(run_duration);
        }
        
        // clean up
        exp_fn.finish();
        
        // progress 2
        #ifdef MAKE_PROGRESS_REPORTS
        if (proc_rank == 0)
        {
            const auto progress_percent = static_cast<std::size_t>(std::ceil(static_cast<double>(exp_counter)
                / static_cast<double>(interleaved_exp_count) * 100.0));
            if (progress_percent > 25 && phase == 0
                || progress_percent > 50 && phase == 1
                || progress_percent > 75 && phase == 2)
            {
                const auto time_stamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
                std::cout << std::put_time(std::localtime(&time_stamp), "[%H:%M:%S]") << " progress just passed "
                    << progress_percent - 1 << "%.\n";
                ++phase;
            }
        }
        #endif
    }
    
    // post-conditions
    if (proc_rank == 0)
    {
        for (const auto& measurement_set : measurements)
        {
            if (measurement_set.size() != exp_count)
            {
                throw std::logic_error("mpi_perform_interleaved_experiments_not_all_experiments_performed_error");
            }
        }
    }
    
    return measurements;
}


//+//////////////////////////////////////////////
// experiment functor
//+//////////////////////////////////////////////

namespace impl
{

class interleaved_comm : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t p_;
    const std::size_t b_;
    const std::size_t proc_rank_;
    
    tensor<double>* tens_recv_buffer_ = nullptr;
    tensor<double>* tens_send_buffer_ = nullptr;
    matrix<double>* mat_recv_buffer_ = nullptr;
    matrix<double>* mat_send_buffer_ = nullptr;
    
    std::size_t dest_rank_tens_ = {};
    std::size_t dest_rank_mat_ = {};
    std::size_t src_rank_tens_ = {};
    std::size_t src_rank_mat_ = {};
    
    std::size_t exp_counter = {};
    
    // cache
    benchmarking::impl::message_packet_details mpd_ = {};
    std::size_t packet_count_times_2_ = {};
    std::vector<MPI_Request> requests_ = {};
    
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    interleaved_comm(const std::size_t& p, const std::size_t& b, const int proc_rank)
        : p_(p), b_(b), proc_rank_(proc_rank)
    {
        // calculate the details
        benchmarking::impl::get_message_packet_details<double>(b * b * b, mpd_);
        
        // create a request array (including requests for the communication of the block matrix)
        requests_.resize(2 * (mpd_.packet_count + 1));
        packet_count_times_2_ = 2 * mpd_.packet_count;
    }
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocation
        tens_recv_buffer_ = new tensor<double>(b_);
        tens_send_buffer_ = new tensor<double>(b_);
        mat_recv_buffer_ = new matrix<double>(b_);
        mat_send_buffer_ = new matrix<double>(b_);
        if (tens_recv_buffer_ == nullptr || tens_send_buffer_ == nullptr || mat_recv_buffer_ == nullptr
            || mat_send_buffer_ ==  nullptr)
        {
            std::cerr << "Fatal error: test data could not be allocated for!" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
        }
        
        // first-touch
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(tens_recv_buffer_->get_n(), 1);
            benchmarking::set_first_touch(*tens_recv_buffer_, work_indices);
            benchmarking::set_first_touch(*tens_send_buffer_, work_indices);
        }
        benchmarking::set_first_touch(*mat_recv_buffer_);
        benchmarking::set_first_touch(*mat_send_buffer_);
        
        // calculation of coordinates in mesh of PEs
        const auto [bi1, bi2, bi3] = get_proc_coords(p_, proc_rank_);
        const auto interleaved_set_index = exp_counter % 3;
        if (interleaved_set_index  == 0)
        {
            dest_rank_tens_ = get_proc_id(p_, bi1, bi2, (bi3 + p_ - 1) % p_); // towards left
            dest_rank_mat_ = get_proc_id(p_, bi1, (bi2 + p_ - 1) % p_, bi3); // towards up
            src_rank_tens_ = get_proc_id(p_, bi1, bi2, (bi3 + p_ + 1) % p_); // from right
            src_rank_mat_ = get_proc_id(p_, bi1, (bi2 + p_ + 1) % p_, bi3); // from below
        }
        else if (interleaved_set_index  == 1)
        {
            dest_rank_tens_ = get_proc_id(p_, bi1, (bi2 + p_ - 1) % p_, bi3); // towards up
            dest_rank_mat_ = get_proc_id(p_, bi1, bi2, (bi3 + p_ - 1) % p_); // towards left
            src_rank_tens_ = get_proc_id(p_, bi1, (bi2 + p_ + 1) % p_, bi3); // from below
            src_rank_mat_ = get_proc_id(p_, bi1, bi2, (bi3 + p_ + 1) % p_); // from right
        }
        else if (interleaved_set_index  == 2)
        {
            dest_rank_tens_ = get_proc_id(p_, (bi1 + p_ - 1) % p_, bi2, bi3); // towards left
            dest_rank_mat_ = get_proc_id(p_, bi1, (bi2 + p_ - 1) % p_, bi3); // towards up
            src_rank_tens_ = get_proc_id(p_, (bi1 + p_ + 1) % p_, bi2, bi3); // from right
            src_rank_mat_ = get_proc_id(p_, bi1, (bi2 + p_ + 1) % p_, bi3); // from below
        }
    }
    
    void perform_experiment() final
    {
        #pragma omp parallel
        {
            if (omp_get_thread_num() == COMM_THREAD)
            {
                for (auto packet_index = std::size_t{}; packet_index < mpd_.packet_count; ++packet_index)
                {
                    const auto message_size = 2 * (packet_index < mpd_.packet_count - 1 ? mpd_.packet_size
                        : mpd_.rest_packet_size);
                    const auto irecv_res = MPI_Irecv(tens_recv_buffer_->data() + packet_index * mpd_.packet_size, message_size,
                        MPI_DOUBLE, src_rank_tens_, 2, MPI_COMM_WORLD, &requests_[2 * packet_index]);
                    const auto isend_res = MPI_Isend(tens_send_buffer_->data() + packet_index * mpd_.packet_size, message_size,
                        MPI_DOUBLE, dest_rank_tens_, 2, MPI_COMM_WORLD, &requests_[2 * packet_index + 1]);
                    if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                    {
                        throw std::runtime_error("tensor_matrix_mult_mpi_sendrecv_error");
                    }
                }
                const auto irecv_res = MPI_Irecv(mat_recv_buffer_->data(), 2 * mat_recv_buffer_->size(), MPI_DOUBLE,
                    src_rank_mat_, 3, MPI_COMM_WORLD, &requests_[packet_count_times_2_]);
                const auto isend_res = MPI_Isend(mat_send_buffer_->data(), 2 * mat_send_buffer_->size(), MPI_DOUBLE,
                    dest_rank_mat_, 3, MPI_COMM_WORLD, &requests_[packet_count_times_2_ + 1]);
                if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                {
                    throw std::runtime_error("tensor_matrix_mult_mpi_sendrecv_error");
                }
                
                // waiting...
                if (MPI_SUCCESS != MPI_Waitall(requests_.size(), requests_.data(), MPI_STATUSES_IGNORE))
                {
                    throw std::runtime_error("tensor_matrix_mult_mpi_waitall_error");
                }
            }
        }
    }
    
    void finish() final
    {
        // force reallocation
        delete tens_recv_buffer_;
        delete tens_send_buffer_;
        delete mat_recv_buffer_;
        delete mat_send_buffer_;
        tens_recv_buffer_ = nullptr;
        tens_send_buffer_ = nullptr;
        mat_recv_buffer_ = nullptr;
        mat_send_buffer_ = nullptr;
        
        // increment the experiment counter
        ++exp_counter;
    }
};

} // namespace impl


int main(int argc, char** argv)
{
    //+//////////////////////////////////////////
    // MPI init
    //+//////////////////////////////////////////

    auto provided_thread_support = int{-1};
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided_thread_support);
    if (provided_thread_support < MPI_THREAD_FUNNELED)
    {
        std::cerr << "Fatal error: Multi-threading support not available with the current MPI implementation." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // obtain rank and size
    auto world_rank = std::numeric_limits<int>::max();
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    auto world_size = -1;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    const auto p_as_fp = std::cbrt(static_cast<double>(world_size));
    const auto p = static_cast<std::size_t>(p_as_fp);
    if (std::fabs(static_cast<double>(p) - p_as_fp) > 1E-15)
    {
        std::cerr << "Fatal error: the number of processes must be cubic." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_SIZE);
    }
    
    //+//////////////////////////////////////////
    // extract problem size
    //+//////////////////////////////////////////
    
    if (argc < 2)
    {
        if (world_rank == 0)
        {
            std::cerr << "Please provide the block size as command line argument." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
    }
    const auto b = static_cast<std::size_t>(std::stoul(argv[1]));
    if (b < 10 || b > 5000)
    {
        if (world_rank == 0)
        {
            std::cerr << "Invalid problem size provided. The program will now be aborted." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
    }
    
    //+//////////////////////////////////////////
    // benchmark runs of communication/computation
    // overlap
    //+//////////////////////////////////////////
    
    // progess
    if (world_rank == 0)
    {
        std::cout << "Starting runs for communication with b=" << b << ", N_node=" << world_size << " (p=" << p << ")"
            << ".\nThread count: " << omp_get_max_threads() << ".\n";
    }
    
    // perform the experiments
    const auto experiment_count = std::size_t{100};
    auto comm_ef = impl::interleaved_comm(p, b, world_rank);
    const auto interleaved_comm_measurements = mpi_perform_interleaved_experiments(MPI_COMM_WORLD, comm_ef, experiment_count, 3);
    
    // compute statistics and write output(s)
    if (world_rank == 0)
    {
        // tensor-matrix multiplication
        const auto measurements_tmm = interleaved_comm_measurements[0];
        const auto comm_stats_tmm = tixl::mpi_compute_statistics(MPI_COMM_WORLD, measurements_tmm);
        tixl::output_results("Benchmarking of communication of tensor-matrix multiplication", comm_stats_tmm, 0,
            b * b * (b + 1) * sizeof(complex<double>));
        benchmarking::write_measurements(measurements_tmm, "comm_tmm_measurements_p" + std::to_string(p) + "_b"
            + std::to_string(b) + ".txt");
        
        // transp. matrix-tensor multiplication
        const auto measurements_tmtm = interleaved_comm_measurements[1];
        const auto comm_stats_tmtm = tixl::mpi_compute_statistics(MPI_COMM_WORLD, measurements_tmtm);
        tixl::output_results("Benchmarking of communication of transp. matrix-tensor multiplication", comm_stats_tmtm, 0,
            b * b * (b + 1) * sizeof(complex<double>));
        benchmarking::write_measurements(measurements_tmtm, "comm_tmtm_measurements_p" + std::to_string(p) + "_b"
            + std::to_string(b) + ".txt");
        
        // transp. tensor-matrix multiplication
        const auto measurements_ttmm = interleaved_comm_measurements[2];
        const auto comm_stats_ttmm = tixl::mpi_compute_statistics(MPI_COMM_WORLD, measurements_ttmm);
        tixl::output_results("Benchmarking of communication of transp. tensor-matrix multiplication", comm_stats_ttmm, 0,
            b * b * (b + 1) * sizeof(complex<double>));
        benchmarking::write_measurements(measurements_ttmm, "comm_ttmm_measurements_p" + std::to_string(p) + "_b"
            + std::to_string(b) + ".txt");
        std::cout << std::endl; // spacing between log dumps makes for better readability
    }
    
    // end
    MPI_Finalize();
    
    return 0;
}
