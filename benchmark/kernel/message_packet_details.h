//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef MESSAGE_PACKET_DETAILS_H
#define MESSAGE_PACKET_DETAILS_H

/// \file This file contains a helper function which calculates the details of sending messages in multiple packets.

#include <utility> // std::size_t
#include <cmath> // std::ceil


namespace s3dft::benchmarking::impl
{

struct message_packet_details
{
    std::size_t packet_count = {};
    std::size_t packet_size = {};
    std::size_t rest_packet_size = {};
};

template<class Treal_t>
void get_message_packet_details(const std::size_t& message_size, message_packet_details& output)
{    
    // all following sizes are in multiples of complex
    static constexpr auto megabyte = 1024 * 1024 / sizeof(complex<Treal_t>);
    static constexpr auto message_packet_size = MESSAGE_PACKET_SIZE * megabyte;
    
    // decide the packet count and rest packet size
    if (message_size <= message_packet_size)
    {
        output.packet_count = 1;
        output.packet_size = 0;
        output.rest_packet_size = message_size;
    }
    else
    {
        output.packet_count = static_cast<std::size_t>(std::ceil(static_cast<double>(message_size)
            / static_cast<double>(message_packet_size)));
        output.packet_size = message_packet_size;
        output.rest_packet_size = message_size % message_packet_size;
    }
    
    #ifndef NDEBUG
    if (output.packet_count < 1)
    {
        throw std::logic_error("impl_get_message_packet_details_invalid_packet_count_error");
    }
    if (output.rest_packet_size > message_size)
    {
        throw std::logic_error("impl_get_message_packet_details_invalid_rest_packet_size_error");
    }
    #endif
}

} // namespace s3dft::benchmarking::impl

#endif
